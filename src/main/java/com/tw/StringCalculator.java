package com.tw;

import java.util.*; 
import java.util.stream.*;

public class StringCalculator {
    private int addLine(String line) {
        if(line.contains("//")) {
            int commentIndex = line.indexOf("//");
            line = line.substring(0, commentIndex);
        }
        String[] numbers = line.split("[;,]");
        int sum = 0;
        for(String number : numbers) {
            if(number != "") {
                sum += Integer.parseInt(number);
            }
        }
        return sum;
    }

    public int add(String string) {
        String[] lines = string.split("[\n]");
        int sum = 0;
        for(String line : lines) {
            sum += addLine(line);
        }
        System.out.println(sum);
        return sum;
    }
}